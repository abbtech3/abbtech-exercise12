package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
    Calculator calculator;

    @BeforeEach
    void init(){
        calculator = new Calculator();
    }

    @Test
    void addTest(){
        double a = 10;
        double b = -4;

        double expectedResult = 6;
        double actualResult = calculator.add(a, b);

        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void subtractTest(){
        double a = 25;
        double b = 10;

        double expectedResult = 15;
        double actualResult = calculator.subtract(a, b);

        // assertEquals seklinde yazmaq olardi assertTrue isletmek ucun bele etdim
        Assertions.assertTrue(expectedResult == actualResult);
    }

    @Test
    void multiplyTest(){
        // inputlari uygun indeksde 2 array seklinde verdim: (1,2),(-2,3),(3,-4),(-4,-5)
        double[] a = {1,-2,3,-4};
        double[] b = {2,3,-4,-5};

        double[] actualResult = new double[4];

        for(int i = 0; i < 4; i++){
            // her bir neticeni uygun indeksde diger array'a yazdim
            actualResult[i] = calculator.multiply(a[i], b[i]);
        }

        // netice bu cur olmalidir
        double[] expectedResult = new double[]{2,-6,-12,20};

        // assertEquals istifade ederek bir input seklinde yaza bilerdim
        // sadece assertArrayEquals isletmek ucun bir nece input seklinde etdim
        Assertions.assertArrayEquals(expectedResult, actualResult);
    }

    @Test
    void divideTest(){
        double a = 5.5;
        double b = 5;

        double expectedResult = 1.1;
        double actualResult = calculator.divide(a, b);

        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void divideByZeroTest(){
        double a = 10.5;
        double b = 0;

        Assertions.assertThrows(ArithmeticException.class, () -> calculator.divide(a, b));
    }

    @Test
    void addTestForMaxValue() {
        double expectedResult = Double.MAX_VALUE;
        double actualResult = calculator.add(Double.MAX_VALUE, 0.0);
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void addTestForMinValue() {
        double expectedResult = Double.MIN_VALUE;
        double actualResult = calculator.add(Double.MIN_VALUE, 0.0);
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void addTestForPositiveInfinity() {
        double expectedResult = Double.POSITIVE_INFINITY;
        double actualResult = calculator.add(Double.POSITIVE_INFINITY, 0.0);
        Assertions.assertEquals(expectedResult, actualResult);
    }


    @Test
    void subtractTestForMaxValue() {
        double expectedResult = Double.MAX_VALUE;
        double actualResult = calculator.subtract(Double.MAX_VALUE, 0.0);
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void subtractTestForMinValue() {
        double expectedResult = Double.MIN_VALUE;
        double actualResult = calculator.subtract(Double.MIN_VALUE, 0.0);
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void subtractTestForPositiveInfinity() {
        double expectedResult = Double.POSITIVE_INFINITY;
        double actualResult = calculator.subtract(Double.POSITIVE_INFINITY, 0.0);
        Assertions.assertEquals(expectedResult, actualResult);
    }


    @Test
    void multiplyTestForMaxValue() {
        double expectedResult = Double.MAX_VALUE;
        double actualResult = calculator.multiply(Double.MAX_VALUE, 1.0);
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void multiplyTestForMinValue() {
        double expectedResult = Double.MIN_VALUE;
        double actualResult = calculator.multiply(Double.MIN_VALUE, 1.0);
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void multiplyTestPositiveInfinity() {
        double expectedResult = Double.POSITIVE_INFINITY;
        double actualResult = calculator.multiply(Double.POSITIVE_INFINITY, 1.0);
        Assertions.assertEquals(expectedResult, actualResult);
    }


    @Test
    void divideTestForMaxValue() {
        double expectedResult = Double.MAX_VALUE;
        double actualResult = calculator.divide(Double.MAX_VALUE, 1.0);
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void divideTestForMinValue() {
        double expectedResult = Double.MIN_VALUE;
        double actualResult = calculator.divide(Double.MIN_VALUE, 1.0);
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void divideTestForPositiveInfinity() {
        double expectedResult = Double.POSITIVE_INFINITY;
        double actualResult = calculator.divide(Double.POSITIVE_INFINITY, 1.0);
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void addTestForTwoInfinity(){
        double a = Double.POSITIVE_INFINITY;
        double b = Double.NEGATIVE_INFINITY;

        double actualResult = calculator.add(a, b);

        Assertions.assertTrue(Double.isNaN(actualResult));
    }

    @Test
    void divideTestForTwoInfinity(){
        double a = Double.POSITIVE_INFINITY;
        double b = Double.NEGATIVE_INFINITY;

        double actualResult = calculator.divide(a, b);

        Assertions.assertTrue(Double.isNaN(actualResult));
    }

    @Test
    void addTestForTwoMax(){
        double a = Double.MAX_VALUE;
        double b = Double.MAX_VALUE;

        double expectedResult = Double.POSITIVE_INFINITY;
        double actualResult = calculator.add(a, b);

        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void multiplyTestForTwoMax(){
        double a = Double.MAX_VALUE;
        double b = Double.MAX_VALUE;

        double expectedResult = Double.POSITIVE_INFINITY;
        double actualResult = calculator.multiply(a, b);

        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void subtractTestForTwoMax(){
        double a = Double.MAX_VALUE;
        double b = Double.MAX_VALUE;

        double expectedResult = 0;
        double actualResult = calculator.subtract(a, b);

        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void divideTestForTwoMax(){
        double a = Double.MAX_VALUE;
        double b = Double.MAX_VALUE;

        double expectedResult = 1;
        double actualResult = calculator.divide(a, b);

        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void addTestForNullValues(){
        Double a = null;
        Double b = null;

        Assertions.assertNull(a);
        Assertions.assertNull(b);
        Assertions.assertThrows(NullPointerException.class, () -> calculator.add(a, b));
    }

    @Test
    void subtractTestForNullValues(){
        Double a = null;
        Double b = null;

        Assertions.assertNull(a);
        Assertions.assertNull(b);
        Assertions.assertThrows(NullPointerException.class, () -> calculator.subtract(a, b));
    }

    @Test
    void multiplyTestForNullValues(){
        Double a = null;
        Double b = 5.5;

        Assertions.assertNull(a);
        Assertions.assertNotNull(b);
        Assertions.assertThrows(NullPointerException.class, () -> calculator.multiply(a, b));
    }

    @Test
    void divideTestForNullValues(){
        Double a = null;
        Double b = 10.0;

        Assertions.assertNull(a);
        Assertions.assertNotNull(b);
        Assertions.assertThrows(NullPointerException.class, () -> calculator.divide(a, b));
    }
}
