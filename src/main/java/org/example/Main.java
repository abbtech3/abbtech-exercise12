package org.example;

public class Main {
    public static void main(String[] args) {
        Calculator c = new Calculator();
        double a = 25.0;
        double b = 10;

        System.out.println("The sum of " + a + " and " + b + " is " + c.add(a, b));
        System.out.println("The product of " + a + " and " + b + " is " + c.multiply(a, b));
        System.out.println("The result of subtraction of " + b + " from " + a + " is " + c.subtract(a, b));
        System.out.println("The result of division of " + a + " to " + b + " is " + c.divide(a, b));
    }
}